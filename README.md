# Kerbal Space Program Remote Controller App
This is a mobile app for both iOS and Android written in TypeScript using React Native and Expo. The app serves as a remote controller for Kerbal Space Program 1 and 2.

## Installation
To use the app, download it from the app stores of your respective phone operating systems.

# Usage
Before using the app, make sure you have the companion app installed on your PC or Mac. You can download the companion app from the link that will be provided later.

Once the companion app is installed, launch Kerbal Space Program and then launch the companion app. From there, follow the instructions to connect your phone to the app. Once your phone is connected, you can launch the Kerbal Space Program Remote Controller App and use it to control your game.

# Features
The app includes the following features:

Control of rockets and spacecraft
Launch control
Thrust control
Maneuvering control
Action Group Button


# Contributing
Contributions are welcome. If you find any issues with the app or want to contribute to its development, please submit a pull request or open an issue on the app's GitHub page.

# License
This app is licensed under the MIT License. See the LICENSE file for more information.