import * as ScreenOrientation from 'expo-screen-orientation'
import { StatusBar } from 'expo-status-bar'
import { Provider as PaperProvider } from 'react-native-paper'
import { SafeAreaProvider } from 'react-native-safe-area-context'
import { ThemeProvider } from 'styled-components'

import Application from './src/main/components/Application'
import { theme } from './src/main/theme/Theme'

export default function App() {
  ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE)
  return (
    <PaperProvider theme={theme}>
      <ThemeProvider theme={theme}>
        <SafeAreaProvider>
          <StatusBar style="light" />
          <Application />
        </SafeAreaProvider>
      </ThemeProvider>
    </PaperProvider>
  )
}
