import { MD3DarkTheme as DefaultTheme } from 'react-native-paper'

export const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#638475',
    secondary: '#DDF093',
    tertiary: '#90E39A',
  },
}
