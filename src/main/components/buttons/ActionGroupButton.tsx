import * as Haptics from 'expo-haptics'
import { useState } from 'react'
import { LayoutChangeEvent, Pressable } from 'react-native'
import { MD3Theme } from 'react-native-paper'
import styled from 'styled-components/native'

interface Props {
  label: string
}

export default function ActionGroupButton({ label }: Props): JSX.Element {
  const [height, setHeight] = useState(30)
  const [pressing, setPressing] = useState(false)
  const onSelfLayout = (e: LayoutChangeEvent) => {
    setHeight(e.nativeEvent.layout.height)
  }

  const onButtonDown = () => {
    Haptics.notificationAsync(Haptics.NotificationFeedbackType.Success)
    setPressing(true)
  }

  const onButtonUp = () => {
    setPressing(false)
  }

  return (
    <Container onLayout={onSelfLayout} layoutHeight={height}>
      <Pressable onPressIn={onButtonDown} onPressOut={onButtonUp}>
        <>
          <Button layoutHeight={height} pressing={pressing} />
          <Label>{label}</Label>
        </>
      </Pressable>
    </Container>
  )
}

const Container = styled.View<{ layoutHeight: number }>`
  height: 100%;
  width: ${props => props.layoutHeight}px;
  flex: 1;
  flex-direction: column;
  padding: 10px;
  align-items: center;
  justify-content: center;
`

const Button = styled.View<{ layoutHeight: number; pressing: boolean; theme: MD3Theme }>`
  height: ${props => props.layoutHeight * 0.7}px;
  width: ${props => props.layoutHeight * 0.7}px;
  align-self: stretch;
  background-color: ${props => (props.pressing ? props.theme.colors.tertiary : props.theme.colors.primary)};
  border-radius: ${props => props.layoutHeight * 0.7}px;
`

const Label = styled.Text`
  color: white;
  align-items: center;
  text-align: center;
`
