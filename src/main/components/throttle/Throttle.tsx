import { forwardRef, useEffect, useImperativeHandle, useMemo, useRef, useState } from 'react'
import {
  Animated,
  Easing,
  GestureResponderEvent,
  LayoutChangeEvent,
  PanResponder,
  PanResponderGestureState,
  Pressable,
  StyleSheet,
} from 'react-native'
import { MD3Theme, useTheme, Text } from 'react-native-paper'
import styled from 'styled-components/native'
type Props = {
  onChange: (val: number) => void
}

export type ThrottleHandle = {
  reset: () => void
}

export default forwardRef<ThrottleHandle, Props>(({ onChange }: Props, ref) => {
  const [height, setHeight] = useState(100)
  const [value, setValue] = useState(0)
  const [offset, setOffset] = useState(0)
  const lastValue = useRef(0)
  const theme = useTheme()

  const onSelfLayout = (e: LayoutChangeEvent) => {
    setHeight(e.nativeEvent.layout.height)
  }

  useImperativeHandle(ref, () => ({
    reset: () => {
      Animated.spring(pan, { toValue: { x: 0, y: -offset }, useNativeDriver: false }).start(() =>
        pan.setOffset({ x: 0, y: 0 })
      )
    },
  }))

  const pan = useRef(new Animated.ValueXY()).current

  const panResponder = useRef(
    PanResponder.create({
      onMoveShouldSetPanResponder: () => true,
      onPanResponderMove: Animated.event([null, { dx: pan.x, dy: pan.y }], { useNativeDriver: false }),
      onPanResponderRelease: (event: GestureResponderEvent, k: PanResponderGestureState) => {
        if (k.moveY > k.y0) {
          Animated.timing(pan, {
            toValue: { x: 0, y: -k.dy },
            duration: 200,
            easing: Easing.ease,
            useNativeDriver: false,
          }).start(() => pan.setOffset({ x: 0, y: 0 }))
        }
        pan.extractOffset()
      },
    })
  ).current

  useEffect(() => {
    pan.addListener(v => {
      setOffset(v.y)
      const val = Math.floor((v.y / (height - 40)) * -100)
      if (val >= 0 && val <= 100 && val !== lastValue.current) {
        lastValue.current = val
        setValue(val)
        onChange(val)
      }
    })

    return () => {
      pan.removeAllListeners()
    }
  }, [height])

  const styles = useMemo(
    () =>
      StyleSheet.create({
        toggle: {
          width: '100%',
          height: 40,
          backgroundColor: theme.colors.tertiary,
          left: 0,
          justifyContent: 'center',
          alignItems: 'center',
        },
      }),
    []
  )

  return (
    <Container>
      <Pressable>
        <Button>
          <Text>FULL</Text>
        </Button>
      </Pressable>
      <ThrottleContainer onLayout={onSelfLayout}>
        <Animated.View {...panResponder.panHandlers} style={[{ transform: [{ translateY: pan.y }] }, styles.toggle]}>
          <Value>{value}</Value>
        </Animated.View>
      </ThrottleContainer>
      <Pressable>
        <Button>
          <Text>FULL</Text>
        </Button>
      </Pressable>
    </Container>
  )
})

const Container = styled.View`
  width: 30%;
  height: 100%;
  padding: 20px;
`

const ThrottleContainer = styled.View<{ theme: MD3Theme }>`
  flex: 1;
  width: 100%;
  background-color: ${({ theme }) => theme.colors.primary};
  justify-content: flex-end;
`

const Value = styled.Text<{ theme: MD3Theme }>`
  color: ${({ theme }) => theme.colors.onTertiary};
  font-weight: bold;
  font-size: 20px;
`

const Button = styled.View<{ theme: MD3Theme }>`
  margin-bottom: 20px;
  margin-top: 20px;
  background-color: ${({ theme }) => theme.colors.primary};
  padding: 16px;
  justify-content: center;
  align-items: center;
`
const ButtonText = styled(Text)<{ theme: MD3Theme }>`
  font-size: 20px;
`
