import { useEffect, useMemo, useRef, useState } from 'react'
import { Animated, LayoutChangeEvent, PanResponder, StyleSheet } from 'react-native'
import { Surface, useTheme, withTheme } from 'react-native-paper'
import styled from 'styled-components/native'

function parseCoordinate(coord: number, width: number): number {
  const knobWidth = width / 4
  const position = 50 + Math.floor((coord / (width / 2 - knobWidth)) * 50)
  return Math.min(Math.max(position, 0), 100)
}

export type Coords = {
  x: number
  y: number
}
type Props = {
  onChange: (coords: Coords) => void
}

export default function Knob({ onChange }: Props): JSX.Element {
  const [width, setWidth] = useState(100)
  const theme = useTheme()
  const onSelfLayout = (e: LayoutChangeEvent) => {
    setWidth(e.nativeEvent.layout.width)
  }

  const pan = useRef(new Animated.ValueXY()).current

  useEffect(() => {
    pan.addListener(value => {
      const x = parseCoordinate(value.x, width)
      const y = parseCoordinate(value.y, width)
      onChange({ x, y })
    })

    return () => {
      pan.removeAllListeners()
    }
  }, [width])

  const styles = useMemo(
    () =>
      StyleSheet.create({
        knob: {
          width: width * 0.5,
          height: width * 0.5,
          backgroundColor: 'black',
          borderRadius: width * 0.5,
        },
      }),
    [width]
  )

  const panResponder = PanResponder.create({
    onStartShouldSetPanResponder: () => true,
    onPanResponderMove: Animated.event(
      [
        null,
        {
          dx: pan.x, // x,y are Animated.Value
          dy: pan.y,
        },
      ],
      { useNativeDriver: false }
    ),
    onPanResponderRelease: () => {
      Animated.spring(
        pan, // Auto-multiplexed
        { toValue: { x: 0, y: 0 }, useNativeDriver: false }
      ).start()
    },
  })

  return (
    <Container onLayout={onSelfLayout} layoutWidth={width}>
      <Animated.View {...panResponder.panHandlers} style={[pan.getLayout(), styles.knob]} />
    </Container>
  )
}

const Container = styled(Surface)<{ layoutWidth: number }>`
  width: 100%;
  height: ${props => props.layoutWidth}px;
  justify-content: center;
  align-items: center;
`
