import * as ScreenOrientation from 'expo-screen-orientation'
import { useRef } from 'react'
import { TouchableHighlight, View } from 'react-native'
import { MD3Theme } from 'react-native-paper'
import { useSafeAreaInsets } from 'react-native-safe-area-context'
import styled from 'styled-components/native'

import ActionGroupButton from './buttons/ActionGroupButton'
import { Text } from './common/Text'
import Knob, { Coords } from './knob/Knob'
import Throttle, { ThrottleHandle } from './throttle/Throttle'
import FlightService from '../services/FlightService'

const flightService = FlightService.instance

export default function Application() {
  const throttleRef = useRef<ThrottleHandle | null>(null)
  ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.LANDSCAPE)
  const insets = useSafeAreaInsets()

  const onThrottleChange = (val: number) => {
    // console.debug(`throttle changed to ${val}`)
    flightService.setThrottle(val)
  }

  const reset = () => {
    throttleRef.current?.reset()
  }

  const onKnobChange = ({ x, y }: Coords) => {
    flightService.setJoystick(x, y)
  }

  return (
    <AppContainer paddingTop={insets.top}>
      <Top>
        <ActionGroupButton label="AG1" />
        <ActionGroupButton label="AG2" />
        <ActionGroupButton label="AG3" />
        <ActionGroupButton label="AG4" />
        <ActionGroupButton label="AG5" />
        <ActionGroupButton label="AG6" />
        <ActionGroupButton label="AG7" />
        <ActionGroupButton label="AG8" />
        <ActionGroupButton label="AG9" />
        <ActionGroupButton label="AG10" />
      </Top>
      <Center>
        <CenterLeft>
          <Throttle onChange={onThrottleChange} ref={throttleRef} />
          <View />
        </CenterLeft>

        <CenterMiddle>
          <TouchableHighlight onPress={reset}>
            <ResetButton>
              <Text>Reset</Text>
            </ResetButton>
          </TouchableHighlight>
          <View />
        </CenterMiddle>
        <CenterRight>
          <View style={{ flex: 1 }} />
          <Knob onChange={onKnobChange} />
        </CenterRight>
      </Center>
    </AppContainer>
  )
}

const AppContainer = styled.View<{ paddingTop: number; theme: MD3Theme }>`
  flex-direction: column;
  width: 100%;
  height: 100%;
  flex: 1;
  justify-content: flex-start;
  align-items: flex-start;
  padding-top: ${props => props.paddingTop}px;
  background-color: ${props => props.theme.colors.surface};
`

const Top = styled.View`
  height: 10%;
  border-bottom-width: 2px;
  border-color: rgba(255, 255, 255, 0.2);
  flex-direction: row;
`

const Center = styled.View`
  flex: 1;
  height: 90%;
  width: 100%;
  flex-direction: row;
`

const CenterLeft = styled.View`
  flex: 1;
  height: 100%;
  border-right-width: 2px;
  border-color: rgba(255, 255, 255, 0.2);
`

const CenterMiddle = styled.View`
  flex: 1;
  height: 100%;
  border-right-width: 2px;
  border-color: rgba(255, 255, 255, 0.2);
`

const CenterRight = styled.View`
  flex: 1;
  height: 100%;
`

const ResetButton = styled.View`
  background-color: darkred;
  margin: 10px;
  padding: 10px;
`
