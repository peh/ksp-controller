import dgram from 'react-native-udp'
import UdpSocket from 'react-native-udp/lib/types/UdpSocket'

/**
 * FlightService is a singleton and FlightService.instance should be used
 */
export default class FlightService {
  public static instance = new FlightService()

  private socket?: UdpSocket
  private host?: string

  private constructor() {
    // should make it hard to create a new FlightService
  }

  public setHost(host: string) {
    this.host = host
    this.init()
  }

  public init(host: string = '192.168.178.25'): void {
    const newSocket = dgram.createSocket({
      type: 'udp4',
      debug: true,
    })
    newSocket.bind()
    newSocket.once('listening', () => {
      newSocket.send('2,reset', undefined, undefined, 5001, host, err => {
        if (err) {
          this.handleError(err)
        } else {
          this.socket = newSocket
        }
      })
    })
  }

  public setJoystick(x: number, y: number) {
    if (!this.socket) {
      return
    }
    this.socket.send(`2,axis,x,${x}`, undefined, undefined, 5001, this.host, this.handleError)
    this.socket.send(`2,axis,y,${y}`, undefined, undefined, 5001, this.host, this.handleError)
  }

  public setThrottle(val: number): void {
    if (!this.socket) {
      return
    }
    this.socket.send(`2,axis,z,${val}`, undefined, undefined, 5001, this.host, this.handleError)
  }

  public async reset(): Promise<void> {
    if (!this.socket) {
      return
    }
    this.socket.send('2,reset', undefined, undefined, 5001, this.host, this.handleError)
  }

  handleError(err?: any) {
    if (err) console.log(`ERR:${err}`)
  }
}
